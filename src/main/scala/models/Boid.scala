package models

import processing.core.{PApplet, PVector}

import scala.annotation.targetName

case class Boid()(using applet: PApplet) {
  import Boid._
  import applet._

  private val radius = 5f
  private val detectionRadius = 100f
  private val fieldAngle = PApplet.radians(120)// PConstants.QUARTER_PI * 3

  val location: PVector = new PVector(random(width.toFloat), random(height.toFloat))
  val velocity: PVector = PVector.random2D.mult(random(3, 6))
  val acceleration: PVector = new PVector(0, 0)

  private def avoidBorders(): Unit = {
    val (desiredVector, d) =
      if (location.x < detectionRadius)
        (new PVector(1, 0), detectionRadius - location.x)
      else if (location.y < detectionRadius)
        (new PVector(0, 1), detectionRadius - location.y)
      else if (location.x > width - detectionRadius)
        (new PVector(-1, 0), detectionRadius + location.x - width.toFloat)
      else if (location.y > height - detectionRadius)
        (new PVector(0, -1), detectionRadius + location.y - height.toFloat)
      else (new PVector(), 0f)

    if (desiredVector.magSq() > 0) {
      val force = desiredVector.steerFrom(velocity).mult(velocity.mag()*0.007f*d)
      acceleration.add(force)
    }
  }

  def canSee(other: Boid): Boolean = {
    def isOtherInView = {
      val relativeDir = PVector.sub(other.location, this.location)

      lazy val a = {
        val m = relativeDir.cross(this.velocity)
        val d = relativeDir.dot(this.velocity)

        // arc tan seems to be more efficient than arc cos (which is used in angleBetween())
        PApplet.atan2(m.mag(), d)
      }
      relativeDir.magSq() < (detectionRadius * detectionRadius)  && a < fieldAngle
    }

    (other ne this) &&
      isOtherInView
  }

  def run(boids: Seq[Boid]): Unit = {
    val (separation, alignment, cohesion) = efficientFlocking(boids)

    separation.mult(1.5f)

    acceleration.add(separation)
    acceleration.add(alignment)
    acceleration.add(cohesion)
  }

  def update(): Unit = {
    avoidBorders()

    velocity.add(acceleration)
    location.add(velocity)

    acceleration.mult(0)
  }

  def display(): Unit = {
    val heading = velocity.heading()

    pushMatrix()

    translate(location.x, location.y)
    rotate(heading)

    noStroke()
    fill(255)
    triangle(
      radius * 2, 0,
      - radius * 2, radius,
      - radius * 2, - radius)

    popMatrix()
  }

  private def efficientFlocking(boids: Seq[Boid]): (PVector, PVector, PVector) = {
    if (boids.nonEmpty) {
      boids.map { other =>
        val distance = PVector.sub(this.location, other.location)
        val dMag = distance.mag()
        distance.setMag(1 / dMag)

        val heading = other.velocity.copy()

        val position = other.location.copy()

        Seq(distance, heading, position)
      }.transpose.map(_.average()) match {
        case Seq(separation, alignment, cohesion) =>
          // Avoid local flock mates
          (separation.steerFrom(velocity),
            // Steer towards local flock heading
            alignment.normalize().steerFrom(velocity),
            // Steer towards local flock center
            cohesion.sub(location).steerFrom(velocity))
      }
    } else (new PVector(0, 0), new PVector(0, 0), new PVector(0, 0))
  }
}

object Boid {
  final val DESIRED_SPEED = 15
  final val MAX_FORCE = 0.2f

  extension (vector: PVector)
    /**
     * @return force vector for steering from `other` to [[vector]]
     */
    def steerFrom(other: PVector): PVector = {
      // Only want desired direction. Desired speed is always same
      // Limit value of final steering force
      vector.setMag(DESIRED_SPEED)
        .sub(other)
        .limit(MAX_FORCE)
    }
    @targetName("add")
    def +(other: PVector): PVector = PVector.add(vector, other)

  extension (vectors: Seq[PVector])
    def average(): PVector = {
      vectors.reduceOption(PVector.add)
        .map(_.div(vectors.size.toFloat))
        .getOrElse(new PVector(0, 0))
    }
}
