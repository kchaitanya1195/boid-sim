package models

import processing.core.PApplet

case class Flock(flockSize: Int)(using PApplet) {
  private val boids: Seq[Boid] = (1 to flockSize).map(_ => Boid())

  def run(): Unit = boids.foreach { boid =>
    val localFlock = boids.filter(boid.canSee)

    if (localFlock.nonEmpty)
      boid.run(localFlock)

    boid.update()
    boid.display()
  }
}
