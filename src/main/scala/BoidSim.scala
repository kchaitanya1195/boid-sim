import models.Flock
import peasy.PeasyCam
import processing.core.{PApplet, PConstants, PFont}

class BoidSim extends PApplet {
  private given BoidSim = this

  // Add PeasyCam jars to lib folder at root
  var cam: PeasyCam = _
  var font: PFont = _
  var flock: Flock = _
  val FLOCK_SIZE = 100

  override def settings(): Unit = size(1280, 720)

  override def setup(): Unit = {
//    cam = new PeasyCam(this, width / 2, height / 2, 0, 100)
//    cam.setWheelScale(0.03)
//    cam.setMaximumDistance(1500)
//    cam.setMinimumDistance(200)
    font = createFont("Arial Bold",48)

    flock = Flock(FLOCK_SIZE)
  }

  override def draw(): Unit = {
    background(0)

    textFont(font,36)
    fill(0xFF00FF00)
    text(frameRate.toInt,20,60)

    flock.run()
  }

  override def mouseClicked(): Unit = flock = Flock(FLOCK_SIZE)

  override def keyPressed(): Unit = {
    key match {
      case ' ' =>
        if (isLooping) noLoop()
        else loop()
      case PConstants.ENTER | PConstants.RETURN =>
        frameCount += 1
        redraw()
      case _ =>
    }
  }
}

object BoidSim extends App {
  PApplet.main("BoidSim")
}