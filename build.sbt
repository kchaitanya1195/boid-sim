name := "boid-sim"

version := "0.1"

scalaVersion := "3.1.1"
scalacOptions ++= Seq("-deprecation")

libraryDependencies += "org.processing" % "core" % "3.3.7"
